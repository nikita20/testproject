<?php


namespace App\Command;


use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UserRoleManagementCommand extends Command
{
    protected static $defaultName = "app:user:roles";
    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct(static::$defaultName);
    }

    protected function configure()
    {
        $this
            ->setDescription("Set roles for users")
            ->setHelp("Add role: {users} {ROLE_USER}. Delete role: —demote {users} {ROLE_ADMIN}")
            ->addOption('demote', null, InputOption::VALUE_OPTIONAL, '', false)
            ->addArgument('username', InputArgument::REQUIRED, 'user')
            ->addArgument('role', InputArgument::REQUIRED, 'role')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $role = $input->getArgument('role');
        $user = $this->em->getRepository(User::class)->findOneBy(['username' => $username]);
        $current_roles = $user->getRoles();

        if ($input->getOption('demote') !== false) {
            $user->removeRole($role);
        } else {
            $user->addRole($role);
        }

        $this->em->flush();

        return 0;
    }

}