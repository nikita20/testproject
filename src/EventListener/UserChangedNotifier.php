<?php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class UserChangedNotifier
{
    public function preUpdate(User $user, PreUpdateEventArgs $eventArgs)
    {
        if($eventArgs->hasChangedField('aboutMe')) {
            $user->setUpdatedAt(new \DateTime());
        }
    }
}