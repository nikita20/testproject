<?php

namespace App\DTO;

use App\Entity\Address;
use App\Entity\Company;
use App\Entity\Skill;
use App\Entity\UserCompany;
use Doctrine\Common\Collections\Collection;

class SettingDataObject
{
    /** @var string|null  */
    public $position;

    /** @var string|null */
    public $aboutMe;

    /** @var Address */
    public $address;

    /** @var Collection|Skill[] */
    public $skills;

    /** @var Collection|UserCompany[] */
    public $companies;

    /**
     * SettingDataObject constructor.
     * @param string|null $position
     * @param string|null $aboutMe
     * @param Address $address
     * @param Collection|Skill[] $skills
     * @param Collection|Company[] $companies
     */
    public function __construct(?string $position, ?string $aboutMe, ?Address $address, $skills, $companies)
    {
        $this->position = $position;
        $this->aboutMe = $aboutMe;
        $this->address = $address;
        $this->skills = $skills;
        $this->companies = $companies;
    }

    /**
     * @return string|null
     */
    public function getPosition(): ?string
    {
        return $this->position;
    }

    /**
     * @param string|null $position
     * @return SettingDataObject
     */
    public function setPosition(?string $position): SettingDataObject
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAboutMe(): ?string
    {
        return $this->aboutMe;
    }

    /**
     * @param string|null $aboutMe
     * @return SettingDataObject
     */
    public function setAboutMe(?string $aboutMe): SettingDataObject
    {
        $this->aboutMe = $aboutMe;
        return $this;
    }

    /**
     * @return Address|null
     */
    public function getAddress(): ?Address
    {
        return $this->address;
    }

    /**
     * @param Address|null $address
     * @return SettingDataObject
     */
    public function setAddress(?Address $address): SettingDataObject
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return Skill[]|Collection
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param Skill[]|Collection $skills
     * @return SettingDataObject
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
        return $this;
    }

    /**
     * @return UserCompany[]|Collection
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * @param UserCompany[]|Collection $companies
     * @return SettingDataObject
     */
    public function setCompanies($companies)
    {
        $this->companies = $companies;
        return $this;
    }

    /**
     * @param UserCompany|null $company
     */
    public function addCompany($company)
    {
        $this->companies->add($company);
    }

    public function removeCompany($company)
    {
        $this->companies->removeElement($company);
    }

    public function addSkill($skill)
    {
        $this->skills->add($skill);
    }

    public function removeSkill(Skill $skill)
    {
        $this->skills->removeElement($skill);
    }
}