<?php

namespace App\Model;


use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

class CompanyModel
{
    private $entityManager;
    private $paginator;
    private $repository;

    public function __construct(EntityManagerInterface $entityManager, PaginatorInterface $paginator, CompanyRepository $companyRepository)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
        $this->repository = $companyRepository;
    }

    public function getCompanySlugBySlug($slug)
    {
        $company = $this->repository->findOneBy(['slug' => $slug]);
        return $company;
    }

    public function getPaginatedUsers($request)
    {
        return $pagination = $this->paginator->paginate(
            $this->repository->getQueryAllCompany(),
            $request->query->getInt('page', 1),
            10
        );
    }
}