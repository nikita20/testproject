<?php

namespace App\Model;

use App\DTO\SettingDataObject;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

class UserModel
{
    private $entityManager;
    private $userRepository;
    private $paginator;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository, PaginatorInterface $paginator)
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    public function saveUserInDb(User $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function getUser($username): User
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['username' => $username]);
        if (!$user) {
            throw new CustomUserMessageAuthenticationException('Username could not be found.');
        } else {
            return $user;
        }
    }

    public function getAllUsers(): array
    {
        return $this->userRepository->findAll();
    }

    public function registerUser(User $user)
    {
        $user->setPosition("")
            ->setRegisteredAt(new \DateTime())
            ->setUpdatedAt(new \DateTime())
            ->setRoles(['ROLE_USER'])
        ;
        $this->saveUserInDb($user);
    }

    public function getPaginatedUsers($request)
    {
        return $pagination = $this->paginator->paginate(
            $this->userRepository->getQueryAllUsers(),
            $request->query->getInt('page', 1),
            10
        );
    }

    public function updateProfileUser(SettingDataObject $settingDTO, int $id)
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['id' => $id]);
        $user->setPosition($settingDTO->position)
            ->setAboutMe($settingDTO->aboutMe)
            ->setAddress($settingDTO->address)
            ->setSkills($settingDTO->getSkills())
        ;

        foreach ($user->getCompanies() as $company) {
            $company->setUser($user);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function deleteUser($user)
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }
}