<?php

namespace App\Controller;

use App\DTO\SettingDataObject;
use App\Entity\User;
use App\Form\UserSettingsType;
use App\Model\UserModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ProfilerController extends AbstractController
{
    public function profile(Request $request, UserModel $userModel)
    {
        /** @var User $user */
        $user = $this->getUser();

        $settingsDTO = new SettingDataObject($user->getPosition(),
            $user->getAboutMe(),
            $user->getAddress(),
            $user->getSkills(),
            $user->getCompanies()
        );

        $form = $this->createForm(UserSettingsType::class, $settingsDTO);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $userModel->updateProfileUser($settingsDTO, $user->getId());
            $this->addFlash(
                'notice',
                'Your changes were saved!'
            );
        }

        return $this->render('user_settings.html.twig', [
            'form' => $form->createView()
        ]);
    }
}