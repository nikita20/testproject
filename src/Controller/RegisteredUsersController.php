<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\UserModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class RegisteredUsersController extends AbstractController
{
    private $userModel;

    public function __construct(UserModel $userModel)
    {
        $this->userModel = $userModel;
    }

    public function registeredUsers(Request $request)
    {
        $pagination = $this->userModel->getPaginatedUsers($request);
        return $this->render('registered_user.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    public function deleteUser(AuthorizationCheckerInterface $authorizationChecker, ?User $user)
    {
        if (!$authorizationChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException();
        }

        if (!$user) {
            throw new NotFoundHttpException();
        }

        $this->userModel->deleteUser($user);
        return $this->redirectToRoute('registered_users');
    }
}
