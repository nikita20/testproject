<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\Company;
use App\Entity\User;
use App\Model\UserModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class MainController
 * @package App\Controller
 */
class MainController extends AbstractController
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function index(Request $request, UserModel $userModel)
    {
     // $this->em->getFilters()->disable("softdeleteable");

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => 22]);
        $address = $this->getDoctrine()->getRepository(Address::class)->findOneBy(['id' => 3]);
        $company = new Company();
        $company->setAddress($address);
        $company->setName("Oracle");
        $company->setDescription("Oracle");
        $company->setFoundedAt(new \DateTime());
        $this->em->persist($company);
        $this->em->flush();
        return $this->render('index.html.twig', []);
    }

    /**
     * @param $mainParameter
     * @return Response
     */
    public function parameters($mainParameter, Request $request)
    {
        $query_request = $request->query;
        if ($query_request->count() > 0) {
            $get_parameters = $query_request->all();
            ksort($get_parameters);
        }
        else
            $get_parameters = null;


        $smile = null;
        if ($mainParameter == "smile1")
            $smile = "😊";
        elseif ($mainParameter == "smile2")
            $smile = "😔";

        return $this->render('parameters.html.twig', [
            'parameters' => $get_parameters,
            'smile' => $smile,
        ]);
    }

    /**
     * @param $invalidParameters
     * @return Response
     */
    public function invalidParameters($invalidParameters): Response
    {
        $response = new Response();
        $random_num = rand();
        $response->setContent($random_num);
        return $response;
    }
}
