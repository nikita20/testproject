<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SignInController extends AbstractController
{

    public function __construct()
    {
    }

    public function signIn(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('sign_in.html.twig', [
            'controller_name' => 'SignInController',
            'error' => $error,
            'last_username' => $lastUsername
        ]);
    }

    public function logOut()
    {
        return $this->redirectToRoute('sign_in');
    }
}
