<?php

namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends EasyAdminController
{
    protected function initialize(Request $request){
        $this->getDoctrine()->getManager()->getFilters()->disable("softdeleteable");
        parent::initialize($request);

    }
}