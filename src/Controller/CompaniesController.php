<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\CompanyModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class CompaniesController extends AbstractController
{
    private $companyModel;

    public function __construct(CompanyModel $companyModel)
    {
        $this->companyModel = $companyModel;
    }

    public function companies(Request $request)
    {
        $pagination = $this->companyModel->getPaginatedUsers($request);

        return $this->render('companies.html.twig', [
            'pagination' => $pagination,
        ]);


    }

    public function company($company_slug)
    {
        $company = $this->companyModel->getCompanySlugBySlug($company_slug);

        return $this->render('company.html.twig', [
            'company' => $company,
        ]);
    }
}