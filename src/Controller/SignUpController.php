<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserRegistrationType;
use App\Model\UserModel;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class SignUpController extends AbstractController
{
    private $userModel;

    public function __construct(UserModel $userModel)
    {
        $this->userModel = $userModel;
    }

    public function signUp(Request $request, MailerService $mailerService)
    {
        $user = new User();

        $form = $this->createForm(UserRegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userName = $user->getUsername();
            $userEmail = $user->getEmail();
            $mailerService->sendMail($userName, $userEmail);
            $this->userModel->registerUser($user);
            return $this->redirectToRoute("registration_completed", ['userEmail' => $userEmail]);
        } else {
            return $this->render('sign_up.html.twig', [
                'form' => $form->createView(),
            ]);
        }
    }

    public function registrationCompleted($userEmail)
    {
        return $this->render('registration_completed.html.twig', [
           'user_mail' => $userEmail,
        ]);
    }
}
