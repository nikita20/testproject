<?php

namespace App\Service;

use Psr\Container\ContainerInterface;

class MailerService
{
    /**
     * @var ContainerInterface
     */
    private $container;
    private $mailer;
    private $twig;

    public function __construct(\Swift_Mailer $mailer, ContainerInterface $container)
    {
        $this->mailer = $mailer;
        $this->container = $container;
        $this->twig = $container->get('twig');
    }

    public function sendMail($username, $email)
    {
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('devzimalab@gmail.com')
            ->setTo('nikita.l@zimalab.com')
            ->setBody(
                $this->twig->render(
                    'mail_registration.html.twig',
                    ['name' => $username]
                ),
                'text/html'
            )
        ;

        $this->mailer->send($message);
    }
}
