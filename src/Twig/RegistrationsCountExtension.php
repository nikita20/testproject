<?php

namespace App\Twig;

use App\Repository\UserRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RegistrationsCountExtension extends AbstractExtension
{
    private $repository;

    public function __construct(UserRepository $userRepository)
    {
        $this->repository = $userRepository;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getRegistrationsCount', [$this, 'getRegistrationsCount']),
        ];
    }

    public function getRegistrationsCount()
    {
        return $this->repository->count(array());
    }
}