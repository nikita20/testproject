<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="users_companies")
 */
class UserCompany
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="companies")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $user;

    /**
     * @var Company|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $company;

    /**
     * @var Department|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Department")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     */
    private $department;

    /**
     * @var DateTime|null;
     * @ORM\Column(name="work_start", type="datetime")
     */
    private $workStart;

    /**
     * @var DateTime|null
     * @ORM\Column(name="work_end", type="datetime")
     */
    private $workEnd;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $position;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $role;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserCompany
     */
    public function setUser(User $user): UserCompany
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Company|null $company
     * @return UserCompany
     */
    public function setCompany(?Company $company): UserCompany
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return Department|null
     */
    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    /**
     * @param Department|null $department
     * @return UserCompany
     */
    public function setDepartment(?Department $department): UserCompany
    {
        $this->department = $department;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getWorkStart(): ?DateTime
    {
        return $this->workStart;
    }

    /**
     * @param DateTime|null $workStart
     * @return UserCompany
     */
    public function setWorkStart(?DateTime $workStart): UserCompany
    {
        $this->workStart = $workStart;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getWorkEnd(): ?DateTime
    {
        return $this->workEnd;
    }

    /**
     * @param DateTime|null $workEnd
     * @return UserCompany
     */
    public function setWorkEnd(?DateTime $workEnd): UserCompany
    {
        $this->workEnd = $workEnd;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPosition(): ?string
    {
        return $this->position;
    }

    /**
     * @param string|null $position
     * @return UserCompany
     */
    public function setPosition(?string $position): UserCompany
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param string|null $role
     * @return UserCompany
     */
    public function setRole(?string $role): UserCompany
    {
        $this->role = $role;
        return $this;
    }

    public function __toString()
    {
        return $this->company." ".$this->workStart->format("Y.M.D");
    }

}