<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\Department;
use App\Entity\UserCompany;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserCompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('company', EntityType::class, [
                    'class' => Company::class,
                    'choice_label' => 'name',
                ])
                ->add('department', EntityType::class, [
                    'class' => Department::class,
                ])
                ->add('workStart', DateTimeType::class, [
                ])
                ->add('workEnd', DateTimeType::class, [
                ])
                ->add('position', TextType::class, [
                ])
                ->add('role', TextType::class, [
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserCompany::class
        ]);
    }
}