<?php

namespace App\Form;

use App\DTO\SettingDataObject;
use App\Entity\Skill;
use App\Entity\UserCompany;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('position', TextType::class, ['required' => false])
                ->add('aboutMe', TextareaType::class, ['required' => false])
                ->add('address', AddressType::class)
                ->add('skills',  CollectionType::class, [
                    'entry_type' => EntityType::class,
                    'entry_options' => [
                        'class' => Skill::class,
                        'choice_label' => 'title',
                        'attr' => ['class' => 'form-control']
                    ],
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false
                ])
                ->add('companies', CollectionType::class, [
                    'entry_type' => UserCompanyType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                ])
                ->add('submit',  SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SettingDataObject::class
        ]);
    }
}