
flash('Default Flash Message', {

        // background color
        'bgColor': '#5cb85c',

        // text color
        'ftColor': 'green',

        // or 'top'
        'vPosition': 'top',

        // or 'left'
        'hPosition': 'right',

        // duration of animation
        'fadeIn': 400,
        'fadeOut': 400,

        // click to close
        'clickable': true,

        // auto hides after a duration time
        'autohide': true,

        // timout
        'duration': 4000

    });