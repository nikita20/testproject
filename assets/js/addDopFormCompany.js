$(function () {
    const $collectionHolder = $('.form-company');
    const companyPrototype = $collectionHolder.data('prototype');
    const counter = $collectionHolder.data('widget-counter');

    $('.add-another-collection-widget').click(function (e) {
        addNewWidget($collectionHolder, companyPrototype, counter);
    });

    $collectionHolder.find('.card-body').each(function() {
        addTagFormDeleteLink($(this));
    });
});

function addNewWidget($holder, prototype, counter) {
    let html = prototype.replace(/__name__/g, counter);
    let $newElem = $('<div class="card" style="margin-top: 20px;" ><div class="card-header">Company</div><div class="card-body">' +  html + '</div>').appendTo($holder);
    $newElem.find('.card-body').each(function() {
        addTagFormDeleteLink($(this));
    });
}

function addTagFormDeleteLink($tagFormLi) {
    var $removeFormButton = $('<button class="btn btn-secondary btn-sm" type="button">Delete</button>');
    $tagFormLi.append($removeFormButton);

    $removeFormButton.on('click', function(e) {
        // remove the li for the tag form
        $tagFormLi.parent().remove();
    });
}